﻿using CaloriesCalculator.App_Start;
using CaloriesCalculator.Models;
using CaloriesCalculator.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;

namespace CaloriesCalculator
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            FilterConfig.RegisterGlobalFilters(GlobalConfiguration.Configuration.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer<DataContext>(null);
            try
            {
                using (var context = new DataContext())
                {
                    if (!context.Database.Exists())
                    {
                        // Create the Meals database without Entity Framework migration schema
                        ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();

                        // Built in user roles
                        var userRoles = new List<UserRole>
                        {
                            new UserRole
                            {
                                Name = "Administrator",
                                Permissions = RwPermissions.RolesRead | RwPermissions.RolesWrite | 
                                RwPermissions.UsersRead | RwPermissions.UsersWrite | 
                                RwPermissions.DataRead | RwPermissions.DataWrite
                            },
                            new UserRole
                            {
                                Name = "User",
                                Permissions = RwPermissions.OwnDataWrite | RwPermissions.OwnDataRead
                            }
                        };
                        context.UserRoles.AddRange(userRoles);
                        context.SaveChanges();

                        // Built in user
                        var user = new User
                        {
                            Name = "admin",
                            Password = AuthenticationService.EncryptPassword("admin"),
                            UserRoleId = "Administrator",
                        };
                        context.Users.Add(user);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Database could not be initialized.", ex);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}