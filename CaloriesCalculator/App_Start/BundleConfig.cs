﻿using System.Web.Optimization;

namespace CaloriesCalculator.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Css
            bundles.Add(new StyleBundle("~/bundles/bootstrap-css")
                .Include("~/Content/css/bootstrap.css")
                .Include("~/Content/css/bootstrap-theme.css")
                );

            bundles.Add(new StyleBundle("~/bundles/angular-material-css").Include(
                "~/Content/css/angular-material.css"
            ));

            bundles.Add(new StyleBundle("~/bundles/application-css")
                .Include("~/Content/css/application.css"));

            // Scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery-js")
                .Include("~/Scripts/jquery-2.2.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-js")
                .Include("~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-js")
               .Include(
                   "~/Scripts/angular.js",
                   "~/Scripts/angular-input-match.js",
                   "~/Scripts/angular-animate/angular-animate.js",
                   "~/Scripts/angular-aria/angular-aria.js",
                   "~/Scripts/angular-messages.js",
                   "~/Scripts/angular-material/angular-material.js,",
                   "~/Scripts/angular-ui-router.js"));

            bundles.Add(new ScriptBundle("~/bundles/caloriesApp")
                .IncludeDirectory("~/app/", "*.js", true));
        }
    }
}