﻿using CaloriesCalculator.Filters;
using System.Web.Http.Filters;

namespace CaloriesCalculator.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(HttpFilterCollection filters)
        {
            filters.Add(new CustomExceptionFilterAttribute());
        }
    }
}