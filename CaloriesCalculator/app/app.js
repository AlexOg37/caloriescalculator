﻿(function () {
    'use strict';

    angular.module('appCal', [
        'ui.router',
        'validation.match',
    ])
    .config([
        '$httpProvider',
        '$urlRouterProvider',
        function ($httpProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/meals');
            $httpProvider.interceptors.push('authenticateHttpInterceptor');
        }
    ]);
})();