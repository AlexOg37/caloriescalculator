﻿(function () {
    'use strict';

    angular
        .module('appCal')
        .directive('lettersAndDigits', lettersAndDigits);

    var lettersAndDigitsRegex = /^[\x20-\x7E\xA0-\xA3\xA5\xA7\xA9-\xB3\xB5-\xB7\xB9-\xBB\xBF-\xFF\u20AC\u0160\u0161\u017D\u017E\u0152\u0153\u0178]+$/;

    function lettersAndDigits() {
        return {
            require: "ngModel",
            restrict: 'A',
            link: function (scope, element, attributes, modelController) {
                modelController.$validators.lettersAndDigits = function (modelValue, viewValue) {
                    if (modelController.$isEmpty(modelValue)) {
                        // An empty model is considered valid.
                        return true;
                    }

                    var isValid = lettersAndDigitsRegex.test(viewValue);
                    return isValid;
                };
            }
        };
    };
})();