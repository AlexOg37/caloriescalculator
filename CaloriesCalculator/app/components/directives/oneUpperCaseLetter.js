﻿(function () {
    'use strict';

    angular
        .module('appCal')
        .directive('oneUpperCase', oneUpperCase);

    var oneUpperCaseRegex = /[A-ZÀ-ÖØ-ÞŒŠŸŽ]+/;

    function oneUpperCase() {
        return {
            require: "ngModel",
            restrict: 'A',
            link: function (scope, element, attributes, modelController) {
                modelController.$validators.oneUpperCase = function (modelValue, viewValue) {
                    if (modelController.$isEmpty(modelValue)) {
                        // An empty model is considered valid.
                        return true;
                    }

                    var isValid = oneUpperCaseRegex.test(viewValue);
                    return isValid;
                };
            }
        };
    };
})();