﻿(function () {
    'use strict';

    angular
        .module('appCal')
        .directive('oneLowerCase', oneLowerCase);

    var oneLowerCaseRegex = /[a-zªµºß-öø-ÿœšž]+/;

    function oneLowerCase() {
        return {
            require: "ngModel",
            restrict: 'A',
            link: function (scope, element, attributes, modelController) {
                modelController.$validators.oneLowerCase = function (modelValue, viewValue) {
                    if (modelController.$isEmpty(modelValue)) {
                        // An empty model is considered valid.
                        return true;
                    }

                    var isValid = oneLowerCaseRegex.test(viewValue);
                    return isValid;
                };
            }
        };
    };
})();