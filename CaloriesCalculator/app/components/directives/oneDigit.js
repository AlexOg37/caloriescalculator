﻿(function () {
    'use strict';

    angular
        .module('appCal')
        .directive('oneDigit', oneDigit);

    var oneDigitRegex = /[0-9]+/;

    function oneDigit() {
        return {
            require: "ngModel",
            restrict: 'A',
            link: function (scope, element, attributes, modelController) {
                modelController.$validators.oneDigit = function (modelValue, viewValue) {
                    if (modelController.$isEmpty(modelValue)) {
                        // An empty model is considered valid.
                        return true;
                    }

                    var isValid = oneDigitRegex.test(viewValue);
                    return isValid;
                };
            }
        };
    };
})();