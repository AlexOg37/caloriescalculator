﻿(function () {
	"use strict";

	angular.module("appCal")
		.controller("mealsController", mealsController);

	mealsController.$inject = ["mealsService", "accountService"];
	function mealsController(mealsService, accountService) {
		var ctrl = this;

		ctrl.meals = [];
		ctrl.isAdding = false;

		ctrl.getMeals = function () {
		    mealsService.getAll()
                .then(function (response) {
                    ctrl.meals = response.data;
                });
		};

		ctrl.delete = function (id) {
		    mealsService.delete(id)
                .then(function (response) {
                    ctrl.getMeals();
                });
		};

		ctrl.showEditDialog = function (id) {
		};

		ctrl.showAddDialog = function () {
		    ctrl.isAdding = true;
		};
		ctrl.closeAddDialog = function () {
		    ctrl.isAdding = false;
		};
		ctrl.okAddDialog = function (meal) {
		    mealsService.add(meal)
                .then(function (response) {
                    ctrl.getMeals();
                });
		    ctrl.closeAddDialog();
		};
		ctrl.cancelAddDialog = function () {
		    ctrl.closeAddDialog();
		};

		ctrl.getMeals();
    };
})();