﻿(function () {
    "use strict";

    angular.module("appCal")
		.service("mealsService", mealsService);

    mealsService.$inject = ['$q', "$http", "$state"];
    function mealsService($q, $http, $state) {
        var api = this;

        api.getAll = function () {
            return $http.get("api/mymeal/", { })
                .then(function (response) {
                    return response;
                })
                .catch(function (reason) {
                    return $q.reject(reason);
                });
        };

        api.add = function (meal) {
            return $http.post("api/mymeal/", meal)
                .then(function (response) {
                    return true;
                })
                .catch(function (reason) {
                    console.log(reason);
                });
        };

        api.edit = function (meal) {
            return $http.put("api/mymeal/", meal)
                .then(function (response) {
                    return true;
                })
                .catch(function (reason) {
                    console.log(reason);
                });
        };

        api.delete = function (id) {
            return $http.delete("api/mymeal/" + id, {})
                .then(function (response) {
                    return true;
                })
                .catch(function (reason) {
                    console.log(reason);
                });
        };
    }
})();
