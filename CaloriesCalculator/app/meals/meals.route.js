﻿(function () {
    "use strict";

    angular.module("appCal")
		.config([
			"$stateProvider",
			function ($stateProvider) {
			    $stateProvider
				    .state("meals", {
				        url: "/meals",
				        controllerAs: "ctrl",
				        templateUrl: "/app/meals/meals.html",
				        controller: "mealsController"
				    })
			}
		]);
})();
