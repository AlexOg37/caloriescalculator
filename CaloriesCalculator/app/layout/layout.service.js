﻿(function () {
    "use strict";

    angular.module("appCal")
		.service("layoutService", layoutService);

    layoutService.$inject = ['$q', "$http"];
    function layoutService($q, $http) {
        var serv = this;
        serv.isLoggedIn = false;
        (function () {
            return $http.get("api/account/ping", {})
                .then(function (response) {
                    serv.isLoggedIn = true;
                })
                .catch(function (reason) {
                    serv.isLoggedIn = false;
                });
        })();
    }
})();
