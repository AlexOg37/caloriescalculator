﻿(function () {
    "use strict";

    angular.module("appCal")
		.controller("layoutController", layoutController);

    layoutController.$inject = ["layoutService", "accountService"];
    function layoutController(layoutService, accountService) {
        var ctrl = this;

        ctrl.isLoggedIn = function () {
            return layoutService.isLoggedIn;
        }

        ctrl.logout = function () {
            layoutService.isLoggedIn = false;
            accountService.logout();
        };
    };
})();