﻿(function () {
    'use strict';

    angular
        .module('appCal')
        .factory('authenticateHttpInterceptor', authenticateHttpInterceptor);

    authenticateHttpInterceptor.$inject = ['$q', '$location', '$rootScope'];

    function authenticateHttpInterceptor($q, $location, $rootScope) {
        return {
            response: function (response) {
                if (typeof response.data.Code != 'undefined') {
                    console.log("Error code: " + response.data.Code);
                    if (response.data.Code == 604) { // wrong pass or name
                    }
                    if (response.data.Code == 606) { // forbidden
                    }
                    return $q.reject(response);
                }
                return response || $q.when(response);
            }
        }
    }
})();