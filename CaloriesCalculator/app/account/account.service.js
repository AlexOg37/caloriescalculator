﻿(function () {
    "use strict";

    angular.module("appCal")
		.service("accountService", accountService);

    accountService.$inject = ['$q', "$http", "$state"];
    function accountService($q, $http, $state) {
        var api = this;

        api.currentUser = null;

        api.logIn = function (userName, password) {
            return $http.post("api/account/login", { userName, password })
                .then(function (response) {
                    api.currentUser = userName;
                    return response;
                })
                .catch(function (reason) {
                    return $q.reject(reason);
                });
        };

        api.registerUser = function (userName, password, confirm) {
            return $http.post("api/account/register", { userName, password, confirm })
                .then(function (response) {
                    api.currentUser = userName;
                    return true;
                })
                .catch(function (reason) {
                    console.log(reason);
                    return false;
                });
        };

        api.logout = function () {
            return $http.post("api/account/logout", {})
                .then(function (response) {
                    $state.go('authorization.login');
                })
                .catch(function (reason) {
                    console.log(reason);
                });
        };
    }
})();
