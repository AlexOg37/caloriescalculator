﻿(function () {
    "use strict";

    angular.module("appCal")
		.controller("registerController", registerController);

    registerController.$inject = ["accountService"];
    function registerController(accountService) {
        var ctrl = this;

        ctrl.register = {
            username: "",
            password: undefined,
            confirmedPassword: undefined
        };

        ctrl.submitRegisterForm = function (form) {
            if (form.$invalid) {
                return;
            }

            accountService.registerUser(ctrl.register.username, ctrl.register.password, ctrl.register.confirmedPassword)
                .then(function (response) {
                    console.log("registered with username '" + ctrl.register.username + "'.");
                });
        };
    };
})();