﻿(function () {
	"use strict";

	angular.module("appCal")
		.controller("loginController", loginController);

	loginController.$inject = ["accountService", "$state", "layoutService"];
	function loginController(accountService, $state, layoutService) {
	    var ctrl = this;
	    ctrl.loginForm = {};

	    ctrl.login = {
	        username: "",
	        password: "",

	        internalError: false,
	        invalidCredentials: false
	    };

	    ctrl.submitLoginForm = function () {
	        ctrl.login.invalidCredentials = false;
	        if (ctrl.loginForm.$invalid) {
	            return;
	        }

	        accountService.logIn(ctrl.login.username, ctrl.login.password)
                .then(function (response) {
                    if (response) {
                        console.log("logged in with username '" + ctrl.login.username + "'.");
                        layoutService.isLoggedIn = true;
                        $state.go('meals');
                    }
                })
    	        .catch(function (reason) {
    	            ctrl.login.invalidCredentials = true;
    	        });
	    };
    };
})();