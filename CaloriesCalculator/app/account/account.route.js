﻿(function () {
	"use strict";

	angular.module("appCal")
		.config([
			"$stateProvider",
			function ($stateProvider) {
			    $stateProvider
                .state("authorization", {
                    'abstract': true,
                    url: "/login",
                    templateUrl: "/app/account/authorization.html"
                })
				    .state("authorization.login", {
					    url: "",
					    controllerAs: "ctrl",
					    templateUrl: "/app/account/login.html",
					    controller: "loginController"
				    })

                    .state("authorization.register", {
                        url: "/register",
                        controllerAs: "ctrl",
                        templateUrl: "/app/account/register.html",
                        controller: "registerController"
                    })
			}
		]);
})();
