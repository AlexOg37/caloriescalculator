﻿using CaloriesCalculator.Models;
using CaloriesCalculator.Services;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CaloriesCalculator.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AuthorizationAttribute : ActionFilterAttribute
    {
        public RwPermissions Permissions { get; set; }

        //public AuthorizationAttribute()
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            RwPermissions permissions;
            var userName = actionContext.Request.Headers.GetValues(ConfigurationManager.AppSettings["UserNameHeader"]).First();
            using (var us = new UserService())
            {
                permissions = us.GetPermissions(userName);
            }
            if (permissions == 0 || !permissions.HasFlag(Permissions))
            {
                throw new CaloriesCalcInternalException(ErrorType.UnknownError);
            }
        }
    }
}