﻿using CaloriesCalculator.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace CaloriesCalculator.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is CaloriesCalcInternalException)
            {
                context.Response = context.Request.CreateResponse(HttpStatusCode.OK,
                    new ErrorResponse
                    {
                        Code = ((CaloriesCalcInternalException)context.Exception).ExceptionType
                    });
                return;
            }
            if (context.Exception is ArgumentNullException ||
                context.Exception is ArgumentException)
            {
                context.Response = context.Request.CreateResponse(HttpStatusCode.OK,
                    new ErrorResponse
                    {
                        Code = ErrorType.BadInputData
                    });
                return;
            }
            context.Response = context.Request.CreateResponse(HttpStatusCode.OK,
                new ErrorResponse
                {
                    Code = ErrorType.UnknownError
                });
        }
    }
}