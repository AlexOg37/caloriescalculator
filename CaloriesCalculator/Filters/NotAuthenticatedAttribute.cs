﻿using CaloriesCalculator.Models;
using CaloriesCalculator.Services;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CaloriesCalculator.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class NotAuthenticatedAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var sessionIdCookie =
                ConfigurationManager.AppSettings["sessionIdCookiePropertyName"];
            CookieHeaderValue cookie = actionContext.Request.Headers.
                GetCookies(sessionIdCookie).FirstOrDefault();
            if (cookie != null)
            {
                var userName = AuthenticationService.
                    GetUserName(cookie[sessionIdCookie].Value);
                if (userName != null)
                    throw new CaloriesCalcInternalException(ErrorType.RecordAlreadyExist);
            }
        }
    }
}