﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CaloriesCalculator.Models
{
    [Table("Meal")]
    public class Meal
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string DateTime { get; set; }
        public string Description { get; set; }
        public int Calories { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
    }
}