﻿namespace CaloriesCalculator.Models
{
    public class UserRegisterModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Confirm { get; set; }
    }
}