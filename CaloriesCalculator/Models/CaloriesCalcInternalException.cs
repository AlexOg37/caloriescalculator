﻿using System;
using System.Runtime.Serialization;

namespace CaloriesCalculator.Models
{
    public class CaloriesCalcInternalException : Exception
    {
        private readonly ErrorType _errorType;
        public ErrorType ExceptionType { get { return _errorType; } private set { } }
        public CaloriesCalcInternalException(ErrorType errorType)
        {
            _errorType = errorType;
        }
        public CaloriesCalcInternalException(ErrorType errorType, string message) : base(message)
        {
            _errorType = errorType;
        }
    }
    public class ErrorResponse
    {
        public ErrorType Code { get; set; }
    }

    public enum ErrorType
    {
        InvalidPassword = 601,
        RecordAlreadyExist,
        NoRecordFound,
        BadInputData,
        UnknownError,
        Unauthorized
    }
}