﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CaloriesCalculator.Models
{
    [Flags]
    public enum RwPermissions : int
    {
        DataRead = 1,
        DataWrite = 2,
        UsersRead = 4,
        UsersWrite = 8,
        RolesRead = 16,
        RolesWrite = 32,
        OwnDataRead = 64,
        OwnDataWrite = 128
    }

    [Table("UserRole")]
    public class UserRole
    {
        [Key]
        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public RwPermissions Permissions { get; set; }
    }
}