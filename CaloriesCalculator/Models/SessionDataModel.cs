﻿using System;

namespace CaloriesCalculator.Models
{
    public class SessionDataModel
    {
        public string UserName { get; set; }
        public DateTime Expiration { get; set; }
    }
}