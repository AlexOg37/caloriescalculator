﻿using System.Data.Entity;

namespace CaloriesCalculator.Models
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Meal> Meals { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Meal>().HasRequired(m => m.User)
                            .WithMany(u => u.Meals)
                            .HasForeignKey(m => m.UserId);
            modelBuilder.Entity<User>().HasRequired(u => u.UserRole)
                            .WithMany(ur => ur.Users)
                            .HasForeignKey(u => u.UserRoleId);
        }
    }
}