﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CaloriesCalculator.Models
{
    [Table("User")]
    public class User
    {
        [Key]
        public string Name { get; set; }
        public string Password { get; set; }
        public string UserRoleId { get; set; }
        public UserRole UserRole { get; set; }
        public virtual ICollection<Meal> Meals { get; set; }
    }
}