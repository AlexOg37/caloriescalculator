﻿using CaloriesCalculator.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace CaloriesCalculator.Services
{
    public class BaseDataService<T>: IDisposable where T : class
    {
        protected DataContext DbContext { get; private set; }
        protected DbSet<T> DbSet { get; private set; }

        public BaseDataService()
        {
            DbContext = new DataContext();
            DbContext.Configuration.ProxyCreationEnabled = false;
            DbContext.Configuration.LazyLoadingEnabled = false;
            DbContext.Configuration.ValidateOnSaveEnabled = false;
            DbSet = DbContext.Set<T>();
        }
        public IQueryable<T> GetAll()
        {
            return DbSet.AsNoTracking();
        }

        public IEnumerable<T> GetFiltered(Expression<Func<T, bool>> filter)
        {
            return DbSet.Where(filter);
        }

        //public T GetById(int id)
        //{
        //    return DbSet.Find(id);
        //}

        public void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }

        }

        public void Update(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;

        }

        public void SetValues(T baseEntity, T entity)
        {
            DbContext.Entry(baseEntity).CurrentValues.SetValues(entity);

        }

        public void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Deleted)
            {
                dbEntityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }

        //public void Delete(int id)
        //{
        //    var entity = GetById(id);
        //    if (entity == null) return;
        //    Delete(entity);
        //}

        public void Commit()
        {
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}