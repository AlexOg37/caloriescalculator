﻿using CaloriesCalculator.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace CaloriesCalculator.Services
{
    public static class AuthenticationService
    {
        private static readonly string salt = "fn2e.0Q5;2p^[*]";

        private static Dictionary<string, SessionDataModel> sessions = 
            new Dictionary<string, SessionDataModel>();

        public static string AddSession(string userName)
        {
            var sessionId = Guid.NewGuid().ToString();
            sessions[sessionId] = new SessionDataModel
            {
                UserName = userName,
                Expiration = DateTime.UtcNow.AddHours(
                    Int32.Parse(
                        ConfigurationManager.AppSettings["sessionDurationInHours"]))
            };
            return sessionId;
        }

        public static string GetUserName(string sessionId)
        {
            if (!IsExpired(sessionId))
            {
                return sessions[sessionId].UserName;
            }
            return null;
        }

        public static bool IsExpired(string sessionId)
            {
            if (!sessions.ContainsKey(sessionId))
                return true;
            if (DateTime.Compare(sessions[sessionId].Expiration, DateTime.UtcNow) <= 0)
                {
                DeleteSession(sessionId);
                return true;
            }
            return false;
                }

        public static bool DeleteSession(string sessionId)
        {
            if (sessions.ContainsKey(sessionId))
            {
                sessions.Remove(sessionId);
                return true;
            }
            return false;
        }
        
        public static string EncryptPassword(string password)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(password + salt));

                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                return sBuilder.ToString();
            }
        }

        public static bool ComparePasswords(string password, string hashedPassword)
        {
            string hashOfInput = EncryptPassword(password);

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hashedPassword))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}