﻿using CaloriesCalculator.Models;
using System.Linq;

namespace CaloriesCalculator.Services
{
    public class UserService: BaseDataService<User>
    {
        public User GetByName(string name)
        {
            return DbSet.FirstOrDefault(u => u.Name.Equals(name));
        }

        public void DeleteByName(string name)
        {
            var entity = GetByName(name);
            if (entity == null)
                throw new CaloriesCalcInternalException(
                    ErrorType.NoRecordFound);
            Delete(entity);
        }

        public RwPermissions GetPermissions(string name)
        {
            var entity = GetByName(name);
            if (entity == null)
                throw new CaloriesCalcInternalException(
                    ErrorType.NoRecordFound);
            return DbContext.Set<UserRole>().FirstOrDefault(ur => string.Equals(ur.Name, entity.UserRoleId))?.Permissions ?? 0;
        }
    }
}