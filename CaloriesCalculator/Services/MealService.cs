﻿using CaloriesCalculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaloriesCalculator.Services
{
    public class MealService: BaseDataService<Meal>
    {
        public Meal GetById(int id)
        {
            return DbSet.FirstOrDefault(u => u.Id == id);
        }

        public void DeleteById(int id)
        {
            var entity = GetById(id);
            if (entity == null)
                throw new CaloriesCalcInternalException(
                    ErrorType.NoRecordFound);
            Delete(entity);
        }
    }
}