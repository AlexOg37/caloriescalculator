﻿using CaloriesCalculator.Filters;
using CaloriesCalculator.Models;
using CaloriesCalculator.Services;
using System.Linq;
using System.Web.Http;

namespace CaloriesCalculator.Controllers
{
    [AuthenticationValidation]
    public class UserController : ApiController
    {
        // GET: api/user
        // Access: CanUsersRead
        [Authorization(Permissions = RwPermissions.UsersRead)]
        public IHttpActionResult Get()
        {
            using (var us = new UserService())
            {
                return Ok(us.GetAll().ToList());
            }
        }

        // GET: api/user/{name}
        // Access: CanUsersRead
        [Authorization(Permissions = RwPermissions.UsersRead)]
        public IHttpActionResult Get(string id)
        {
            using (var us = new UserService())
            {
                var user = us.GetByName(id);
                if (user == null)
                    throw new CaloriesCalcInternalException(
                        ErrorType.NoRecordFound);
                return Ok(user);
            }
        }

        // POST: api/user
        // Access: CanUsersWrite
        [Authorization(Permissions = RwPermissions.UsersWrite)]
        public IHttpActionResult Post(User model)
        {
            User user = new User
            {
                Name = model.Name,
                Password = AuthenticationService.EncryptPassword(model.Password),
                UserRoleId = model.UserRoleId
            };
            using (var us = new UserService())
            {
                if (us.GetByName(user.Name) != null)
                    throw new CaloriesCalcInternalException(
                        ErrorType.RecordAlreadyExist,
                        $"The user with name {user.Name} already exists in database.");
                us.Add(user);
                us.Commit();
                return Ok();
            }
        }

        // PUT: api/user
        // Access: CanUsersWrite
        [Authorization(Permissions = RwPermissions.UsersWrite)]
        public IHttpActionResult Put(User model)
        {
            using (var us = new UserService())
            {
                var user = us.GetByName(model.Name);
                if (user == null)
                    throw new CaloriesCalcInternalException(
                        ErrorType.NoRecordFound);
                if (!string.Equals(model.Password.ToLower(), user.Password.ToLower()))
                    model.Password = AuthenticationService.EncryptPassword(model.Password);
                us.Update(model);
                us.Commit();
                return Ok();
            }
        }

        // DELETE: api/user/{name}
        // Access: CanUsersWrite
        [Authorization(Permissions = RwPermissions.UsersWrite)]
        public IHttpActionResult Delete(string id)
        {
            using (var us = new UserService())
            {
                us.DeleteByName(id);
                us.Commit();
                return Ok();
            }
        }
    }
}