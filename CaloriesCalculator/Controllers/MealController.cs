﻿using CaloriesCalculator.Filters;
using CaloriesCalculator.Models;
using CaloriesCalculator.Services;
using System;
using System.Linq;
using System.Web.Http;

namespace CaloriesCalculator.Controllers
{
    [AuthenticationValidation]
    public class MealController : ApiController
    {
        // GET: api/meal
        // Access: CanDataRead
        [Authorization(Permissions = RwPermissions.DataRead)]
        public IHttpActionResult Get()
        {
            using (var ms = new MealService())
            {
                return Ok(ms.GetAll().ToList());
            }
        }

        // GET: api/meal/{id}
        // Access: CanDataRead
        [Authorization(Permissions = RwPermissions.DataRead)]
        public IHttpActionResult Get(int id)
        {
            using (var ms = new MealService())
            {
                var meal = ms.GetById(id);
                if (meal == null)
                    throw new CaloriesCalcInternalException(ErrorType.NoRecordFound);
                return Ok(meal);
            }
        }

        // POST: api/meal
        // Access: CanDataWrite
        [Authorization(Permissions = RwPermissions.DataWrite)]
        public IHttpActionResult Post(Meal model)
        {
            Meal meal = new Meal
            {
                Description = model.Description ?? string.Empty,
                Calories = model.Calories,
                DateTime = DateTime.UtcNow.ToString(),
                UserId = model.UserId
            };
            using (var ms = new MealService())
            {
                ms.Add(meal);
                ms.Commit();
                return Ok();
            }
        }

        // PUT: api/meal
        // Access: CanDataWrite
        [Authorization(Permissions = RwPermissions.DataWrite)]
        public IHttpActionResult Put(Meal meal)
        {
            using (var ms = new MealService())
            {
                if (ms.GetById(meal.Id) == null)
                    throw new CaloriesCalcInternalException(
                        ErrorType.NoRecordFound);
                ms.Update(meal);
                ms.Commit();
                return Ok();
            }
        }

        // DELETE: api/meal/{name}
        // Access: CanDataWrite
        [Authorization(Permissions = RwPermissions.DataWrite)]
        public IHttpActionResult Delete(int id)
        {
            using (var ms = new MealService())
            {
                ms.DeleteById(id);
                ms.Commit();
                return Ok();
            }
        }
    }
}