﻿using CaloriesCalculator.Filters;
using CaloriesCalculator.Models;
using CaloriesCalculator.Services;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace CaloriesCalculator.Controllers
{
    [AuthenticationValidation]
    public class MyMealController : ApiController
    {
        private string _userName
        {
            get
            {
                var res = Request.Headers.GetValues(
                    ConfigurationManager.AppSettings["UserNameHeader"]).First();
                using (var us = new UserService())
                {
                    if (us.GetByName(res) == null)
                        throw new CaloriesCalcInternalException(
                            ErrorType.NoRecordFound);
                }
                return res;
            }
        }
        // GET: api/mymeal
        // Access: CanOwnDataRead
        [Authorization(Permissions = RwPermissions.OwnDataRead)]
        public IHttpActionResult Get()
        {
            using (var ms = new MealService())
            {
                return Ok(ms.GetFiltered(m => m.UserId.Equals(_userName)).ToList());
            }
        }

        // GET: api/mymeal/{id}
        // Access: CanOwnDataRead
        [Authorization(Permissions = RwPermissions.OwnDataRead)]
        public IHttpActionResult Get(int id)
        {
            using (var ms = new MealService())
            {
                var meal = ms.GetById(id);
                if (meal == null || meal.UserId != _userName)
                    throw new CaloriesCalcInternalException(ErrorType.NoRecordFound);
                return Ok(meal);
            }
        }

        // POST: api/mymeal
        // Access: CanOwnDataWrite
        [Authorization(Permissions = RwPermissions.OwnDataWrite)]
        public IHttpActionResult Post(Meal model)
        {
            Meal meal = new Meal
            {
                Description = model.Description ?? string.Empty,
                Calories = model.Calories,
                DateTime = DateTime.UtcNow.ToString(),
                UserId = _userName
            };
            using (var ms = new MealService())
            {
                ms.Add(meal);
                ms.Commit();
                return Ok();
            }
        }

        // PUT: api/mymeal
        // Access: CanOwnDataWrite
        [Authorization(Permissions = RwPermissions.OwnDataWrite)]
        public IHttpActionResult Put(Meal meal)
        {
            if (meal.UserId != _userName)
                throw new CaloriesCalcInternalException(ErrorType.NoRecordFound);
            using (var ms = new MealService())
            {
                if (ms.GetById(meal.Id) == null)
                    throw new CaloriesCalcInternalException(
                        ErrorType.NoRecordFound);
                ms.Update(meal);
                ms.Commit();
                return Ok();
            }
        }

        // DELETE: api/mymeal/{name}
        // Access: CanOwnDataWrite
        [Authorization(Permissions = RwPermissions.OwnDataWrite)]
        public IHttpActionResult Delete(int id)
        {
            using (var ms = new MealService())
            {
                var meal = ms.GetById(id);
                if (meal == null || meal.UserId != _userName)
                    throw new CaloriesCalcInternalException(ErrorType.NoRecordFound);
                ms.Delete(meal);
                ms.Commit();
                return Ok();
            }
        }
    }
}