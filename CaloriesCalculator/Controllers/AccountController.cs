﻿using CaloriesCalculator.Filters;
using CaloriesCalculator.Models;
using CaloriesCalculator.Services;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace CaloriesCalculator.Controllers
{
    public class AccountController : ApiController
    {
        [HttpPost]
        [Route("api/account/login")]
        [NotAuthenticated]
        public HttpResponseMessage Login(UserLoginModel model)
        {
            var resp = new HttpResponseMessage();

            if (model == null)
                throw new ArgumentNullException("The user name or password provided is incorrect.");
            if (String.IsNullOrWhiteSpace(model.UserName) || String.IsNullOrWhiteSpace(model.Password))
                throw new ArgumentNullException("The user name or password provided is incorrect.");

            User user = null;
            using (var us = new UserService())
            {
                user = us.GetByName(model.UserName);
                if (user == null || !AuthenticationService.ComparePasswords(model.Password, user.Password))
                    throw new ArgumentNullException("The user name or password provided is incorrect.");
            }

            var sessionId = AuthenticationService.AddSession(user.Name);

            var cookie = new CookieHeaderValue(
                ConfigurationManager.AppSettings["sessionIdCookiePropertyName"], 
                sessionId);
            cookie.Expires = DateTimeOffset.Now.AddHours(
                Int32.Parse(ConfigurationManager.AppSettings["sessionDurationInHours"]));
            cookie.Domain = Request.RequestUri.Host;
            cookie.Path = "/";

            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });
            return resp;
        }

        [HttpGet]
        [Route("api/account/ping")]
        [AuthenticationValidation]
        public IHttpActionResult PingSession()
        {
            return Ok();
        }

        [HttpPost]
        [Route("api/account/logout")]
        [AuthenticationValidation]
        public IHttpActionResult Logout()
        {
            var sessionIdCookie =
                ConfigurationManager.AppSettings["sessionIdCookiePropertyName"];
            CookieHeaderValue cookie = Request.Headers.
                GetCookies(sessionIdCookie).FirstOrDefault();
            if (cookie != null)
            {
                AuthenticationService.DeleteSession(
                    cookie[sessionIdCookie].Value);
            }
            return Ok();
        }

        [HttpPost]
        [Route("api/account/register")]
        [NotAuthenticated]
        public IHttpActionResult Register(UserRegisterModel model)
        {
            var resp = new HttpResponseMessage();

            if (model == null)
                throw new ArgumentNullException("The registration data provided is incorrect.");
            if (String.IsNullOrWhiteSpace(model.UserName) || 
                String.IsNullOrWhiteSpace(model.Password) ||
                String.IsNullOrWhiteSpace(model.Confirm))
                throw new ArgumentNullException("The registration data provided is incorrect.");
            if (!String.Equals(model.Password, model.Confirm))
                throw new ArgumentException("The password and confirmation password do not match.");

            using (var us = new UserService())
            {
                if (us.GetByName(model.UserName) != null)
                {
                    throw new CaloriesCalcInternalException(
                        ErrorType.RecordAlreadyExist, 
                        $"The user with name {model.UserName} already exists in database.");
                }
                us.Add(new User
                {
                    Name = model.UserName,
                    Password = AuthenticationService.EncryptPassword(model.Password),
                    UserRoleId = "User"
                });
                us.Commit();
            }

            return Ok();
        }
    }
}